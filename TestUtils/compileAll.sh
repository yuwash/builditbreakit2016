find . -name src -type d >> javaSrcPaths
for D in `cat javaSrcPaths`
do ( cd $D
    mkdir -p ../bin
    grep -lr 'void main' . >> javaMainFiles
    xargs -d '\n' -a javaMainFiles javac -d ../bin
)
done
