export TARGET=$PWD
while read D
do (
    cd $D/../bin
    pwd
    find -name Main.class | sed 's/^\.\///; s/\//./g; s/\.class$//' | \
        xargs -I'{}' jar cef '{}' "$TARGET/`cd ..; basename $PWD`_`git rev-parse HEAD`.jar" *
)
done < javaGoodSrcPaths
