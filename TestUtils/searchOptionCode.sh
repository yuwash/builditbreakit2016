while read D
do (
	cd $D
	pwd
	xargs -d '\n' -a javaMainFiles grep -Hn -e '"-o"'
)
done < javaSrcPaths
