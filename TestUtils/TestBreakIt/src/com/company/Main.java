package com.company;
import java.io.IOException;
import java.lang.Runtime;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
   String workingdir = "C:\\Users\\yuliia\\Documents\\Visual Studio 2013\\builditbreakit2016\\out\\production\\builditbreakit2016\\";
        String testdir = "C:\\Users\\yuliia\\Documents\\Visual Studio 2013\\builditbreakit2016\\TestCases\\";
     //  String workingdir = "C:\\Users\\yuliia\\Downloads\\Mathe Dual Teams\\no_more_names_please\\bin";


        //TEAM "   String workingdir =  "C:\\Users\\yuliia\\Downloads\\Mathe Dual Teams\\break_this\\out\\production\\break_this\\";
         // String workingdir =  "C:\\Users\\yuliia\\Downloads\\Mathe Dual Teams\\mathedual-wettbewerb-2016\\out\\production\\mathedual-wettbewerb-2016\\";
          //      String workingdir = "C:\\Users\\yuliia\\Downloads\\Mathe Dual Teams\\getrekt\\bin\\";

        for(int i = 0 ; i<50; i++){


            String containers = String.format("tc%04d.containers.txt", i);
            String ships = String.format("tc%04d.ships.txt", i);
            String output = String.format("tc%04d.output.txt", i);
            String expected = String.format("tc%04d.expected.txt", i);
            String readme = String.format("tc%04d.readme.txt", i);

            File f = new File(testdir + containers);
            if(f.exists()) {
                executeTEST(workingdir, testdir, containers, ships, output, expected, readme);
            }
        }
    }
private static void  executeTEST(String workingdir, String testdir, String container, String ship, String output, String expected , String readme) throws IOException , FileNotFoundException{


    try {
        //builditbreakit2016. builditbreakit2016  getrekt.challenge16. de.hahnjo.matheDualWettbewerb2016   //String workingdir = "C:\\Users\\yuliia\\Documents\\Visual Studio 2013\\builditbreakit2016\\out\\production\\builditbreakit2016\\";
        Process p = Runtime.getRuntime().exec(new String[]{"java ", "builditbreakit2016.Main",
                "-c", testdir+container,
                "-s", testdir+ship,
                "-o", testdir+output,}, null, new File(workingdir) );
        //  "java' builditbreakit2016.Main  ' -s 'C:\\Users\\yuliia\\Documents\\Visual Studio 2013\\builditbreakit2016\\out\\production\\builditbreakit2016\\schiff.txt' -o 'C:\\Users\\yuliia\\Documents\\Visual Studio 2013\\builditbreakit2016\\out\\production\\builditbreakit2016\\output.txt'");
        p.waitFor();
       /* p = Runtime.getRuntime().exec(new String[]{"java ", "builditbreakit2016.Main",
                "-s", ship,
                "-c", container,
                "-o", output,}, null, new File(workingdir) );
                p.waitFor();
        p = Runtime.getRuntime().exec(new String[]{"java ", "builditbreakit2016.Main",
                "-o", output,
                "-c", container,
                "-s", ship,
                }, null, new File(workingdir) );
                p.waitFor();*/

        // Test if output equals with expected
        Scanner outputSc = new Scanner(new File(testdir + output));
        Scanner expectedSc = new Scanner(new File(testdir + expected));
        boolean errorTest = false;



        int iLine=0;

        while(expectedSc.hasNextLine() && outputSc.hasNextLine()) {
            String lineOut = outputSc.nextLine();
            String lineExp = expectedSc.nextLine();
            if (iLine == 0)
            {
                iLine++;
                String cutLineOut = lineOut.substring(0, 3);
                String cutlineExp = lineExp.substring(0, 3);
                if (cutlineExp.equals("!!!"))
                {

                    if (!cutLineOut.equals(cutlineExp)) {
                        System.out.println("!!! " + output + " != " + expected);
                        errorTest = true;
                        Scanner readmeSc = new Scanner(new File(testdir + readme));
                        while (readmeSc.hasNextLine()) {
                            System.out.println(readmeSc.nextLine());
                        }
                        break;
                    }
                    else    {
                        errorTest = true;
                        System.out.println(output + " == " + expected);
                        break;
                    }
                }
            }


                if ( !lineOut.equals(lineExp)){
                    System.out.println("!!! " + output + " != " + expected);
                    errorTest = true;
                    Scanner readmeSc = new Scanner(new File(testdir + readme));
                    while(readmeSc.hasNextLine()){
                        System.out.println(readmeSc.nextLine());
                    }
                    break;
                }



        }

        if( errorTest == false ){
            System.out.println(output + " == " + expected);
        }
        else {

        }
        outputSc.close();
        expectedSc.close();

        String line;

        BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        while((line = error.readLine()) != null){
            System.out.println(line);
        }
        error.close();

        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while((line=input.readLine()) != null){
            System.out.println(line);
        }

        input.close();

        OutputStream outputStream = p.getOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        printStream.println();
        printStream.flush();
        printStream.close();
    } catch (Exception ex) {
        ex.printStackTrace();
    }
}}
