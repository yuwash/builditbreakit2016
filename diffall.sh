export REFERENCE=TestCases/output/builditbreakit2016/
OUT_DIRS=( TestCases/output/break_this/ TestCases/output/mathedualkrusebengsch/ TestCases/output/programming-contest/ )
export REL_NAME="`basename ${1%.jar}`"
(
    cd $REFERENCE/..
    if [ -e "$REL_NAME" ]
    then echo outputdirectory already exists, exiting
        exit
    else mkdir "$REL_NAME"
        ln -siT "$REL_NAME" ../../$REFERENCE
    fi
)
(
    cd $REFERENCE
    ../../../TestUtils/testall.sh ../../../"$1"
)
for t in ${OUT_DIRS[@]}
do TARGET="$t/../ours_to_`basename $t`.diff"
    diff -w $REFERENCE $t > "$TARGET"
    python differrclean.py < "$TARGET" > "${TARGET%.diff}_errclean.diff"
done
