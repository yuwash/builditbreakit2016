#!/usr/bin/env python
from __future__ import print_function
import sys

lines = sys.stdin.readlines()

found = False
buffer = []
for i in xrange(len(lines)): 
  if(lines[i][:4]=='diff'):
    if(not found):
      for line in buffer:
        print(line, end='')
    found = False
    buffer = []
  elif(lines[i][:5]=='< !!!'): 
    found = True
  elif(lines[i]=='---'):
    if(lines[i+1][:5]!='> !!!'):
      found = False
  buffer.append(lines[i])
       
if(not found):
  for line in buffer:
    print(line, end='')
