package builditbreakit2016;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class Main {
    private static ByteArrayOutputStream errorBaos = new ByteArrayOutputStream();
    public static final PrintStream err = new PrintStream(errorBaos);
    private static ByteArrayOutputStream outBaos = new ByteArrayOutputStream();
    public static final PrintStream out = new PrintStream(outBaos);
    
    private static final boolean REALLY_QUIET = true;
    private static final boolean SINGLE_LINE_ERRORS = true;
    private static final String DELIMITER = ";";
    private static final String ERROR_FORMAT = "!!! %s !!!";

    public static void main(String[] args) {

        //! wenn du testen willst!
        //System.out.println("Working Directory = " +
        //		System.getProperty("user.dir"));

        // fall-back values
        String containerFName = "tc0001.containers.txt";
        String schiffFName = "tc0001.ships.txt";
        String outputFName = "tc0001.output.txt";

        //TEST IF NUMBER OF PARAMETERS IS CORRECT
        if(args.length == 0){
        	if(!REALLY_QUIET){
        		System.err.println("No arguments given, using fall-back values");
        	}
        }else if(args.length%2 != 0){
        	if(!REALLY_QUIET){
        		System.err.println("Error: arguments has to be of even number, "+args.length+" arguments given");
        	}
        	return;
        }else if(args.length!=6){
        	if(!REALLY_QUIET){
	        	System.err.println(
	        		"Warning: there should be normally 6 arguments. Any duplicate arguments are overwritten."
	        	);
        	}
        }
        
        // command-line options
        for (int h = 0; h < args.length/2; h++) {
        	int i = 2*h;
            if (args[i].equals("-c")) {
                containerFName = args[i + 1];
                //i++;
            } else if (args[i].equals("-s")) {
                schiffFName = args[i + 1];
                //i++;
            } else if (args[i].equals("-o")) {
                outputFName = args[i + 1];
                //i++;
            } else {
            	if(!REALLY_QUIET){
            		System.err.println("unrecognized argument "+args[i]+" ignored");
            	}
            }
        }

        File containerF = new File(containerFName);
        File schiffF = new File(schiffFName);
        File outputF = new File(outputFName);



        ContainerHandler containerFH = new ContainerHandler(containerF);
        SchiffHandler schiffFH = new SchiffHandler(schiffF);
        try {


            ArrayList<Container> containers = containerFH.read();
            ArrayList<Schiff> schiffe = schiffFH.read();

            if (Main.errorBaos.size() > 0)
            {
                Scanner errSc = new Scanner(Main.errorBaos.toString());
                while(errSc.hasNextLine()){
                	String line = errSc.nextLine();
                	if(line.length()>1024){
                		line = line.substring(0,1024);
                	}
                    out.print(String.format(ERROR_FORMAT, line));
                    if(!SINGLE_LINE_ERRORS){
                    	out.println();
                    }
                }
                errSc.close();
                PrintWriter outputPw = new PrintWriter(outputF);
                outputPw.print(Main.outBaos);
                outputPw.close();
                return;
            }


            algorithmus(schiffe, containers);

            Scanner errSc = new Scanner(Main.errorBaos.toString());
            while(errSc.hasNextLine()){
            	String line = errSc.nextLine();
            	if(line.length()>1024){
            		line = line.substring(0,1024);
            	}
                out.print(String.format(ERROR_FORMAT, line));
                if(!SINGLE_LINE_ERRORS){
                	out.println();
                }
            }
            errSc.close();

            PrintWriter outputPw = new PrintWriter(outputF);
            outputPw.print(Main.outBaos);
            outputPw.close();



        } catch (FileNotFoundException e) {
            e.printStackTrace();/*
		} catch (IOException e) {
			e.printStackTrace();*/
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Double> accumulate(ArrayList<Double> entries) {
        ArrayList<Double> result = new ArrayList<>();
        double sum = 0;
        for (double e : entries) {
            sum += e;
            result.add(sum);
        }
        return result;
    }


    public static void algorithmus(ArrayList<Schiff> schiffe, ArrayList<Container> container) {
//		FileWriter fw = new FileWriter(outputF);
//		BufferedWriter bw = new BufferedWriter(fw);
        ArrayList<Container> containerGut = new ArrayList<Container>();
        int i = 0; //Container index
        int j = 0;
        int h = 0;
        boolean fail = false; // prevent endless loop
        while(container.size()>0){
            for ( j = 0; j<schiffe.size(); j++){ //shiff
                ArrayList<String> allShip = schiffe.get(j).getPorts();
                for(h = 0; h<allShip.size(); h++){ //hafen von schiff
                    if(container.size() ==0){
                        break;
                    }
                    String zielHafen = container.get(i).getHafen();
                    if(zielHafen.equals(allShip.get(h))){  //container ziel haffen und shiffhaffen passt
                        if(schiffe.get(j).getAvailableVolumen() >= container.get(i).getVolume() ) { //container valume passt
                            if(schiffe.get(j).getAvailableGewicht() >= container.get(i).getGewicht()){
                                if(schiffe.get(j).getTravelTime(h) <= container.get(i).getLieferDatum()){
                                    schiffe.get(j).consume(container.get(i).getVolume(), container.get(i).getGewicht());
                                    schiffe.get(j).addContainerID(container.get(i).getId());
                                    containerGut.add(container.get(i));
                                    container.remove(i);
                                    j = -1;
                                    h = 0;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // if shipment could not be realized
            if(container.size()!=0){
                container.get(i).setHafen("Aachen");
                containerGut.add(container.get(i));
                container.remove(i);
            }

        }


        // sort containers
        Collections.sort(containerGut);
        // sort schiffe
        ArrayList<Schiff> schiffeGut = new ArrayList<>(schiffe);
        Collections.sort(schiffeGut);
        //NICHT LÖSCHEN
		/*
			Collections.sort(containerGut, new Comparator<Container>() {
				@Override
				public int compare(Container left, Container right) {
					int leftLength = left.getId().length();
					int rightLength = right.getId().length();
					int ka = 0;
					int kb = 0;
					while (true) {
						if (ka == leftLength) {
							return kb == rightLength ? 0 : -1;
						}
						if (kb == rightLength) {
							return 1;
						}
						if (left.getId().charAt(ka) >= '0' && left.getId().charAt(ka) <= '9' && right.getId().charAt(kb) >= '0' && right.getId().charAt(kb) <= '9') {
							int na = 0;
							int nb = 0;
							while (ka < leftLength && left.getId().charAt(ka) == '0') {
								ka++;
							}
							while (ka + na < leftLength && left.getId().charAt(ka + na) >= '0' && left.getId().charAt(ka + na) <= '9') {
								na++;
							}
							while (kb < rightLength && right.getId().charAt(kb) == '0') {
								kb++;
							}
							while (kb + nb < rightLength && right.getId().charAt(kb + nb) >= '0' && right.getId().charAt(kb + nb) <= '9') {
								nb++;
							}
							if (na > nb) {
								return 1;
							}
							if (nb > na) {
								return -1;
							}
							if (ka == leftLength) {
								return kb == rightLength ? 0 : -1;
							}
							if (kb == rightLength) {
								return 1;
							}

						}
						if (left.getId().charAt(ka) != right.getId().charAt(kb)) {
							return left.getId().charAt(ka) - right.getId().charAt(kb);
						}
						ka++;
						kb++;
					}
				}
			});
*/

        //NICHT LÖSCHEN
			/*
			Collections.sort(schiffeGut, new Comparator<Schiff>() {
				@Override
				public int compare(Schiff left, Schiff right) {
					int leftLength = left.getId().length();
					int rightLength = right.getId().length();
					int ka = 0;
					int kb = 0;
					while (true) {
						if (ka == leftLength) {
							return kb == rightLength ? 0 : -1;
						}
						if (kb == rightLength) {
							return 1;
						}
						if (left.getId().charAt(ka) >= '0' && left.getId().charAt(ka) <= '9' && right.getId().charAt(kb) >= '0' && right.getId().charAt(kb) <= '9') {
							int na = 0;
							int nb = 0;
							while (ka < leftLength && left.getId().charAt(ka) == '0') {
								ka++;
							}
							while (ka + na < leftLength && left.getId().charAt(ka + na) >= '0' && left.getId().charAt(ka + na) <= '9') {
								na++;
							}
							while (kb < rightLength && right.getId().charAt(kb) == '0') {
								kb++;
							}
							while (kb + nb < rightLength && right.getId().charAt(kb + nb) >= '0' && right.getId().charAt(kb + nb) <= '9') {
								nb++;
							}
							if (na > nb) {
								return 1;
							}
							if (nb > na) {
								return -1;
							}
							if (ka == leftLength) {
								return kb == rightLength ? 0 : -1;
							}
							if (kb == rightLength) {
								return 1;
							}

						}
						if (left.getId().charAt(ka) != right.getId().charAt(kb)) {
							return left.getId().charAt(ka) - right.getId().charAt(kb);
						}
						ka++;
						kb++;
					}
				}
			});
*/

        // container output
// container output

        for (int z = 0; z < containerGut.size(); z++) {
            String id = "" + containerGut.get(z).getId();
            out.println(id + DELIMITER + containerGut.get(z).getHafen() + DELIMITER);
        }
        out.println("###");



        // ship and their containers output
        for (Schiff s : schiffeGut) {
            String id = s.getId();
            ArrayList<String> getContainerID = s.getContainerID();
            out.print(id + DELIMITER);
            for (int g = 0; g < getContainerID.size(); g++) {
                out.print(getContainerID.get(g).toString() + DELIMITER );
            }
            out.println();
        }
    }
}
