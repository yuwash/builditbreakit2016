package builditbreakit2016;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class DoubleArrayFileHandler implements SimpleArrayFileHandler<ArrayList<Double>> {
	private File file;

	public DoubleArrayFileHandler(File file) {
		this.file = file;
	}

	@Override
	public ArrayList<ArrayList<Double>> read() throws FileNotFoundException {
		ArrayList<ArrayList<Double>> result = new ArrayList<>();
		Scanner fSc = new Scanner(this.file);
		while(fSc.hasNextLine()){
			String line = fSc.nextLine();
			String[] entries = line.split(",");
			ArrayList<Double> numbers = new ArrayList<>();
			for(String e: entries){
				try{
					numbers.add(Double.valueOf(e));
				}catch(NumberFormatException err){
					System.err.println("There was an error converting this to double: \""+err+"\"");
				}
			}
			result.add(numbers);
		}
		fSc.close();
		return result;
	}

	@Override
	public void write(ArrayList<ArrayList<Double>> data) throws FileNotFoundException {
		PrintWriter fPw = new PrintWriter(this.file);
		for(ArrayList<Double> numbers: data){
			ArrayList<String> entries = new ArrayList<>();
			for(double n: numbers){
				entries.add(String.valueOf(n));
			}
			String line;
			line = String.join(",", entries);
			fPw.println(line);
		}
		fPw.close();
	}

}