package builditbreakit2016;

import java.util.ArrayList;
import java.util.Collection;

import builditbreakit2016.WordString.WordFormatException;


public class Schiff extends StringIdObject {
	private int ankunftZielHafenZeit;
	private ArrayList<String> ports;
	private ArrayList<Integer> travelTimes;
	private ArrayList<String> containerID = new ArrayList<String>();
	private final double maxVolumen;
	private final double maxGewicht;
	private double availableVolumen;
	private double availableGewicht;


	public Schiff(String id, Collection<Schiff> uniquenessCollection, double maxVolumen, double maxGewicht, int AbfahrtZeit, ArrayList<String> ports, ArrayList<Integer> times) throws WordFormatException {
		super(id, uniquenessCollection);
		// id is handled by the superclass!
		this.ports = ports;
		this.travelTimes = times;

		//Rechnen Zeit für die Fahrt
		for (int i = 1; i < times.size(); i++) {
			times.set(i, times.get(i) + times.get(i - 1));
		}
		for (int i = 0; i < times.size(); i++) {
			travelTimes.set(i, times.get(i) + AbfahrtZeit);
		}

		if(0 <= maxVolumen){
			this.maxVolumen = maxVolumen;
			this.resetAvailableVolumen();
		}else{
			throw new RuntimeException("Schiff: maxVolumen has to be bigger than 0 or 0  .");
		}

		if(0 <= maxGewicht  ){
			this.maxGewicht = maxGewicht;
			this.resetAvailableGewicht();
		}else{
			throw new RuntimeException("Schiff: maxGewicht has to be bigger than 0 or 0 .");
		}
	}

	public int getTravelTime(int i) {
		return travelTimes.get(i);
	}

	public void addContainerID(String id) {

		containerID.add(id);

	}

	/**
	 * @return the containerID
	 */
	public ArrayList<String> getContainerID() {
		return containerID;
	}


	/**
	 * @return the ports
	 */
	public ArrayList<String> getPorts() {
		return ports;
	}

	/**
	 * @return the travelTimes
	 */
	public ArrayList<Integer> getTravelTimes() {

		return travelTimes;
	}

	/**
	 * @return the maxVolumen
	 */
	public double getMaxVolumen() {

		return maxVolumen;
	}

	/**
	 * @return the maxGewicht
	 */
	public double getMaxGewicht() {
		return maxGewicht;
	}

	public void consume(double volumen, double gewicht){
		if(0<=volumen && 0<=gewicht){
			if(volumen<=this.availableVolumen && gewicht<=this.availableGewicht){
				this.availableVolumen -= volumen;
				this.availableGewicht -= gewicht;
			}else{
				throw new RuntimeException(
						"Schiff: not enough capacity available! Volume has to be below " + this.availableVolumen
								+ " m^3, Weight has to be below " + this.availableGewicht + " t; " + volumen + " m^3 and "
								+ gewicht + " t given"
				);
			}
		}else{
			throw new RuntimeException(
					"Schiff: can only consume non-negatie volume and weight; " + volumen + " m^3 and "
							+ gewicht + " t given"
			);
		}
	}

	/**
	 * @return the availableVolumen
	 */
	public double getAvailableVolumen() {
		return availableVolumen;
	}

	/**
	 * @return the availableGewicht
	 */
	public double getAvailableGewicht() {
		return availableGewicht;
	}

	public void resetAvailableVolumen() {
		this.availableVolumen = this.maxVolumen;
	}

	public void resetAvailableGewicht() {
		this.availableGewicht = this.maxGewicht;
	}

	/**
	 * @return the ankunftZielHafenZeit
	 */
	public int getAnkunftZielHafenZeit() {
		return ankunftZielHafenZeit;
	}

	/**
	 * @param ankunftZielHafenZeit the ankunftZielHafenZeit to set
	 */
	public void setAnkunftZielHafenZeit(int ankunftZielHafenZeit) {
		this.ankunftZielHafenZeit = ankunftZielHafenZeit;
	}
}