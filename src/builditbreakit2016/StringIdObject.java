package builditbreakit2016;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import builditbreakit2016.WordString.WordFormatException;

public abstract class StringIdObject implements Comparable<StringIdObject> {
	protected final String id;
	private Collection<? extends StringIdObject> uniquenessCollection;
	private final List<Character> ADDITIONAL_CHARS = Arrays.asList(
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_'
	);

	public StringIdObject(String id, Collection<? extends StringIdObject> uniquenessCollection) throws WordFormatException {
		this.uniquenessCollection = uniquenessCollection;
		this.id = WordString.valueOf(id, ADDITIONAL_CHARS);
		if(this.uniquenessCollection.contains(this.id)){
			throw new RuntimeException("id "+this.id+" already taken");
		}
	}

	@Override
	public int compareTo(StringIdObject o) {
		return this.id.compareTo(o.id);
	}

    /**
     * @return the id
     */
    public String getId() {
        return this.id;
    }
}
