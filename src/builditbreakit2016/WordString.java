package builditbreakit2016;

import java.util.List;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class WordString {
	public static class WordFormatException extends Exception {

		/**
		 * Just call constructor of Exception
		 */
		public WordFormatException() {
			super();
			// TODO Auto-generated constructor stub
		}

		/**
		 * Just call constructor of Exception
		 * @param message   the detail message for Exception
		 */
		public WordFormatException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}

		/**
		 * generated serialVersionUID
		 */
		private static final long serialVersionUID = -7111287009400065443L;
	}

	private static final int MIN_LENGTH = 1;
	private static final int MAX_LENGTH = 64;
	private static final List<Character> ALLOWED_CHARS = Arrays.asList(
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
			'-', '.'
	);

	public static String valueOf(String s) throws WordFormatException {
		return valueOf(s, Collections.emptyList());
	}

	public static String valueOf(String s, Collection<Character> additionalChars) throws WordFormatException {
		String result = s.trim();
		int n = result.length();
		if (MIN_LENGTH <= n && n <= MAX_LENGTH) {/*
			for (int i = 0; i < result.length(); i++) {
				char c = result.charAt(i);
				if (!ALLOWED_CHARS.contains(c) && !additionalChars.contains(c)) {
					//Main.errorString += "Character " + c + "  not allowed";
					throw new WordFormatException("Character " + c + " not allowed");
				}
			}*/
		} else {
			throw new WordFormatException(
					"Word has to be of length [" + MIN_LENGTH + ".." + MAX_LENGTH + "]; \"" + result + "\" given"
			);
		}
		return result;
	}

}
