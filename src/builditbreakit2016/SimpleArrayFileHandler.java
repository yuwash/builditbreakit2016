package builditbreakit2016;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public interface SimpleArrayFileHandler<E> {
	public ArrayList<E> read() throws FileNotFoundException;
	public void write(ArrayList<E> data) throws FileNotFoundException;
}