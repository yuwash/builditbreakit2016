package builditbreakit2016;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class SchiffHandler implements SimpleArrayFileHandler<Schiff> {
	private static final int MAX_FILE_SIZE = 2048; // in Bytes 
	private File file;
	private static final String DELIMITER = "\\s*;\\s*"; // to ignore surrounding whitespaces
	private static final boolean REQUIRE_DESTTIME = true;

	/*
	 * Schiffe Schiffe haben jeweils eine ID, ein maximales Ladevolumen, ein
	 * maximales Ladegewicht, eine Abfahrtszeit und mindestens einen Zielhafen
	 * mit Angabe der Reisedauer vom vorherigen Hafen in Sekunden dorthin.
	 */

	public SchiffHandler(File file) {
		if(file.length() <= MAX_FILE_SIZE){
			this.file = file;
		}else{
			throw new RuntimeException(
					"Schiff: file too big; only files up to "+MAX_FILE_SIZE+" B supported."
			);
		}
	}

	@Override
	public ArrayList<Schiff> read() throws FileNotFoundException {
		ArrayList<Schiff> result = new ArrayList<>();
		Scanner fSc = new Scanner(this.file);
		while (fSc.hasNextLine()) {
			String line = fSc.nextLine();
			if(line.isEmpty()){
				continue;
			}
			if(line.charAt(line.length()-1)!=';'){
				Main.err.println("Schiff: line has to end with a \";\"");
			}
			String[] entries = line.split(DELIMITER);
			String id;
			//String hafen;
			int AbfahrtZeit;
			// int kapazitaet;
			double maxVolumen;
			double maxGewicht;
			// 2;2;500;1453909814;Hamburg (7200);Kiel (3600);
			if (entries.length >= 5) {
				try {
					/* id uniqueness is checked in the constructor of container */
					if( checkNewId(entries[0] , result)){
						id = entries[0];
					}
					else{
						break;
					}

					if(Double.valueOf(entries[1]) > 0 ){
						maxVolumen = Double.valueOf(entries[1]);
					}
					else{
						throw new RuntimeException("Volume has to be positiv; \"" + entries[1] + "\" given.");
					}

					if(  Double.valueOf(entries[2]) >0 ) {

						maxGewicht = Double.valueOf(entries[2]);
					}
					else{
						throw new RuntimeException("Container´s Heavy has to be positiv; \"" + entries[2] + "\" given.");

					}

					if(Integer.valueOf(entries[3]) > 0 ){
						AbfahrtZeit = Integer.valueOf(entries[3]);
					}
					else{
						throw new RuntimeException("Departure time has to be positiv; \"" + entries[3] + "\" given.");

					}

					ArrayList<Integer> arrTimes = new ArrayList<Integer>();
					ArrayList<String> arrPorts = new ArrayList<String>();

					for (int h = 4; h < entries.length; h++) {
						String zeile = entries[h];
						int arrTime = 0;
						String port;

						// ignores anything after the last ')'
						int clammer2 = zeile.lastIndexOf(')');
						int clammer1 = zeile.lastIndexOf('(', clammer2);
						
						if(clammer1 != -1 && clammer2 != -1){
							String arrTimeStr = zeile.substring(clammer1 + 1, clammer2).replaceAll("\\s", "");
							arrTime = parseUnixTime(arrTimeStr); // integer testing happens here
							port = zeile.substring(0, clammer1).trim();
						}else if(clammer1 != -1 || clammer2 != -1){
							throw new RuntimeException("inconsistent brackets for destination " + zeile);
						}else if(!REQUIRE_DESTTIME){
							port = zeile.trim();
							arrTime = Integer.MAX_VALUE;
						}else{
							throw new RuntimeException("missing destination time for " + zeile.trim());
						}

						arrTimes.add(arrTime);
						arrPorts.add(WordString.valueOf(port));
					}

					Schiff item = new Schiff(id, result, maxVolumen, maxGewicht, AbfahrtZeit, arrPorts, arrTimes);
					result.add(item);
				} catch (NumberFormatException err) {
					Main.err.print("Schiff: error converting: " + err.getMessage());
				} catch (Exception err) {
					Main.err.println("Schiff: " + err.getMessage());
				}
			} else {
				Main.err.println("Schiff must contain min 5 data: " + line + " given.");
			}
		}
		fSc.close();
		return result;
	}

	@Override
	public void write(ArrayList<Schiff> data) throws FileNotFoundException {
		// TODO Auto-generated method stub

	}
	//
	private static boolean checkNewId(String id, ArrayList<Schiff> schiffe) {
		for (Schiff s : schiffe) {
			if (s.getId().equals(id)) {
				throw new RuntimeException("Id has to be doppelt given");
			}
		}
		return true;
	}

	private static int parseUnixTime(String timeStr){
		try {
			long time = Long.parseLong(timeStr);
			if(0<=time && time < Integer.MAX_VALUE){
				return (int) time;
			}else{
				throw new RuntimeException(
						"time has to be non-negative and smaller than "+Integer.MAX_VALUE+"; "+timeStr+" given."
				);
			}
		} catch (NumberFormatException err) {
			throw new RuntimeException("time has to be integer; \"" + timeStr + "\" given.");
		}
	}

}