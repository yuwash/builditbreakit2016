package builditbreakit2016;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class ContainerHandler implements SimpleArrayFileHandler<Container> {
	private File file;
	//	private boolean append = false;
	private static final String DELIMITER = "\\s*;\\s*";

	public ContainerHandler(File file) {
		this.file = file;
	}

	@Override
	public ArrayList<Container> read() throws FileNotFoundException {
		ArrayList<Container> result = new ArrayList<>();
		Scanner fSc = new Scanner(this.file);
		while(fSc.hasNextLine()){
			String line = fSc.nextLine();
			if(line.isEmpty()){
				continue;
			}
			if(line.charAt(line.length()-1)!=';'){
				Main.err.println("Container: line has to end with a \";\"");
				continue;
			}
			String[] entries = line.split(DELIMITER);
			String id;
			String hafen;
			double hoehe;
			double breite;
			double laenge;
			double gewicht;
			int lieferDatum;
			if(entries.length == 7){
				try{
					/* id uniqueness is checked in the constructor of container */
					if( checkNewId(entries[0],result )){
						id = entries[0];
					}
					else{
						break;
					}

					hafen = WordString.valueOf(entries[1]);
					hoehe = Double.valueOf(entries[2]);
					breite = Double.valueOf(entries[3]);
					laenge = Double.valueOf(entries[4]);
					gewicht = Double.valueOf(entries[5]);

					lieferDatum = Integer.valueOf(entries[6]);
					if (lieferDatum <=0 ){
						throw new RuntimeException("Date has to be bigger than 0; \"" + lieferDatum + "\" given.");
					}
					Container item = new Container(id, result, hafen, hoehe, breite, laenge, gewicht, lieferDatum);
					result.add(item);
				}catch(NumberFormatException err){
					Main.err.println("Container: There was an error converting this to integer: \""+err+"\"");
				}catch(Exception err){
					Main.err.println("Contaner: " + err.getMessage());
				}
			}else{
				Main.err.println("Container must contain exactly 7 data, " + entries.length + " entries given");
			}
		}
		fSc.close();
		return result;
	}


	private static boolean checkNewId(String id, ArrayList<Container> container) {
		for (Container c : container) {
			if (c.getId().equals(id)) {
				throw new RuntimeException("Id has to be doppelt given");
			}
		}
		return true;
	}

	@Override
	public void write(ArrayList<Container> data) throws FileNotFoundException {
		PrintWriter fPw = new PrintWriter(this.file);
		for(Container item: data){
			String[] entries = {
					String.format("%d", item.getId()),
					item.getHafen()
			};
			String line = String.join(DELIMITER, entries) + DELIMITER;
			fPw.println(line);
		}
		fPw.close();
	}
}