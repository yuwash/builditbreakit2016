package builditbreakit2016;

import java.util.Collection;

import builditbreakit2016.WordString.WordFormatException;

public class Container extends StringIdObject {
	private String hafen;
	private final double high;
	private final double breite;
	private final double laenge;
	private double gewicht;
	private double volume;
	private int lieferDatum;

	public Container(String id, Collection<Container> uniquenessCollection, String hafen, double high, double breite, double laenge, double gewicht, int lieferDatum) throws WordFormatException {
		super(id, uniquenessCollection);
		// id is handled by the superclass!

		this.hafen = hafen;

		if (0 < high ) {
			this.high = high;
		} else {
			throw new RuntimeException("Container: high has to be bigger than 0");
		}

		if (0 < breite) {
			this.breite = breite;
		} else {
			throw new RuntimeException("Container: breite has to be  has to be bigger than 0");
		}

		if (0 < laenge ) {
			this.laenge = laenge;
		} else {
			throw new RuntimeException("Container: laenge has to be  has to be bigger than 0");
		}

		if (0 < gewicht) {
			this.gewicht = gewicht;
		} else {
			throw new RuntimeException("Container: gewicht has to be  has to be bigger than 0");
		}

		// those numbers already checked
		this.volume = breite * laenge * high;
		this.lieferDatum = lieferDatum;
	}

	/**
	 * @return the hoehe
	 */
	public double getHigh() {
		return high;
	}

	public void setLieferDatum(int datum) {
		this.lieferDatum = datum;
	}

	/**
	 * @return the lieferDatum
	 */
	public int getLieferDatum() {
		return lieferDatum;
	}

	/**
	 * @return the breite
	 */
	public double getBreite() {
		return breite;
	}

	/**
	 * @return the volume
	 */
	public double getVolume() {
		return volume;
	}

	/**
	 * @param volume
	 *            the volume to set
	 */
	public void setVolume(double volume) {
		this.volume = volume;
	}

	/**
	 * @return the laenge
	 */
	public double getLaenge() {
		return laenge;
	}

	/**
	 * @return the gewicht
	 */
	public double getGewicht() {
		return gewicht;
	}



	/**
	 * @return the hafen
	 */
	public String getHafen() {
		return hafen;
	}

	/**
	 * @param hafen
	 *            the hafen to set
	 */
	public void setHafen(String hafen) {
		this.hafen = hafen;
	}
}
