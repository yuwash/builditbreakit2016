# Builditbreakit2016

This project contains our submission of the builditbreakit project described in [mathedual2016 / Organisatorisches](https://bitbucket.org/mathedual2016/organisatorisches) and some testing utilities.

## Usage

java builditbreakit2016.Main -c  tc0001.containers.txt -s tc0001.ships.txt -o tc0001.output.txt

## Test cases

For the break phase, our test cases for submission are located in the `TestCases` subdirectory. The `TestCases/ReadMe.txt` gives you an overview of the test cases and the resp. `tc####.readme.txt` contains, if applicable, notes about failing teams.
