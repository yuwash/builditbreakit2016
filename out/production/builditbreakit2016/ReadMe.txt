0001. Doppelt ID in Ships 
0002. Doppelt ID in Containers
0003. Leerzeichnen statt ID in Containers
0004. Leerzeichnen statt ID in Ships 


Test Case 0005 - 0011
0005. Minus ID in Ships
0006. ID mit Ziffer und Buchstabe in Ships
0007. ID nur mit Buchstaben in Ships
0008. ID nur mit Spezielen Zeichnen in Ships 
0009. ID mit Buchstabe und Spezielen Zeichen wie \t in Ships 
0010. Leerzeicnen vor ID in Ships 
0011. Leerzeichnen nach ID in Ships 

Test Case 00012 - 0018
0012. Minus ID in Containers
0013. ID mit Ziffer und Buchstabe in Containers
0014. ID nur mit Buchstaben in Containers
0015. ID nur mit Spezielen Zeichnen in Containers
0016. ID mit Buchstabe und Spezielen Zeichnen wie \t in Containers
0017. Leerzeicnen vor ID in Containers 
0018. Leerzeichnen nach ID in Containers

